package examples

import javax.ws.rs.GET
import javax.ws.rs.Path

import org.eclipse.jetty.server.Server
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.servlet.ServletHolder

import com.sun.jersey.api.core.PackagesResourceConfig
import com.sun.jersey.spi.container.servlet.ServletContainer

@Path("/")
class HelloWorld {

	@GET
	def String say() {
		return "Hello JAX-RS by Groovy";
	}

	def static void main(String[] args) {
		int port = 8080;
		String p = System.getenv("PORT");
		if (p != null && 0 < p.length()) {
			port = Integer.valueOf(p);
		}
		
		PackagesResourceConfig config = new PackagesResourceConfig("examples");
		ServletContainer container = new ServletContainer(config);
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
		context.setContextPath("/");
		context.addServlet(new ServletHolder(container), "/*");
		Server server = new Server(port);
		server.setHandler(context);
		server.start();
		server.join();
	}
}
